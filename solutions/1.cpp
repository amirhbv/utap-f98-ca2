#include <iostream>
#include <cmath>
using namespace std;

int convertBase6ToBase10(string s)
{
    int size = s.size();
    if (!size)
    {
        return 0;
    }
    return (s[0] - '0') * pow(6, size - 1) + convertBase6ToBase10(s.substr(1));
}

int main()
{
    string s;
    while(cin >> s) {
        cout << convertBase6ToBase10(s) << endl;
    }
    return 0;
}
