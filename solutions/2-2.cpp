#include <iostream>
#include <vector>

using namespace std;

vector<int> readInput()
{
    vector<int> numbers;
    int temp;
    while(cin >> temp)
        numbers.push_back(temp);
    return numbers;
}

vector<int> sortedInsert(vector<int> numbers, int number)
{
    if (numbers.size() == 0 || number > numbers.back())
    {
        numbers.push_back(number);
        return numbers;
    }

    int lastNumber = numbers.back();
    numbers.pop_back();

    numbers = sortedInsert(numbers, number);

    numbers.push_back(lastNumber);
    return numbers;
}

vector<int> recursiveSort(vector<int> numbers)
{
    if (numbers.size() == 0)
        return numbers;

    int lastNumber = numbers.back();
    numbers.pop_back();

    numbers = recursiveSort(numbers);

    numbers = sortedInsert(numbers, lastNumber);
    return numbers;
}

void printVector(vector<int> numbers, int size)
{
    if (numbers.size() == 0)
        return;

    int lastNumber = numbers.back();
    numbers.pop_back();

    printVector(numbers, size);
    cout << lastNumber;

    numbers.push_back(lastNumber);
    if (numbers.size() == size)
        cout << endl;
    else
        cout << " ";
}

int main()
{
    vector<int> numbers = readInput();
    numbers = recursiveSort(numbers);
    printVector(numbers, numbers.size());
    return 0;
}
