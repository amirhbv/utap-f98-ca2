#include <iostream>
#include <vector>

using namespace std;

vector<int> readInput()
{
    vector<int> numbers;
    int number;
    while(cin >> number)
        numbers.push_back(number);
    return numbers;
}

vector<int> insertAtFirst(vector<int> numbers, int number)
{
    if (numbers.size() == 0)
    {
        numbers.push_back(number);
        return numbers;
    }

    int lastNumber = numbers.back();
    numbers.pop_back();

    numbers = insertAtFirst(numbers, number);

    numbers.push_back(lastNumber);
    return numbers;
}

vector<int> reverse(vector<int> numbers)
{
    if (numbers.size() == 0)
        return numbers;

    int lastNumber = numbers.back();
    numbers.pop_back();

    numbers = reverse(numbers);

    numbers = insertAtFirst(numbers, lastNumber);
    return numbers;
}

void printVector(vector<int> numbers, int size)
{
    if (numbers.size() == 0)
        return;

    int lastNumber = numbers.back();
    numbers.pop_back();

    printVector(numbers, size);
    cout << lastNumber;

    numbers.push_back(lastNumber);
    if (numbers.size() == size)
        cout << endl;
    else
        cout << " ";
}

int main()
{
    vector<int> numbers = readInput();
    numbers = reverse(numbers);
    printVector(numbers, numbers.size());
    return 0;
}
