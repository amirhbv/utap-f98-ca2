//
// Created by Mohsen on 10/6/2019.
//

#include <iostream>
#include <vector>
#include <string>

#define BLANK_CHAR '.'
#define MINE_CHAR 'M'
#define HIDDEN_CHAR '*'

std::vector<std::string>* readInputMap(int numberOfRows){
    std::vector<std::string>* mineMap = new std::vector<std::string>();
    std::string inputRow;
    for (int i = 0; i < numberOfRows; ++i) {
        std::cin >> inputRow;
        mineMap->push_back(inputRow);
    }
    return mineMap;
}

void printMap(std::vector<std::string>* map){
    for (unsigned int i = 0; i < map->size(); ++i) {
        std::cout << (*map)[i] << std::endl;
    }
}

bool isPointInMapRange(std::vector<std::string>* mineMap, int x, int y){
    return (x >= 0 && x < (*mineMap)[0].length() && y >= 0 && y < mineMap->size());
}

bool isMineHere(std::vector<std::string>* mineMap, int x, int y){
    if(isPointInMapRange(mineMap, x, y)){
        if((*mineMap)[y][x] == MINE_CHAR){
            return true;
        }
    }
    return false;
}

int numberOfNeighborMines(std::vector<std::string>* mineMap, int x, int y){
    int neighborMinesCounter = 0;
    if(isMineHere(mineMap, x+1, y))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x, y+1))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x+1, y+1))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x-1, y))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x, y-1))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x-1, y-1))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x+1, y-1))
        neighborMinesCounter++;
    if(isMineHere(mineMap, x-1, y+1))
        neighborMinesCounter++;

    return neighborMinesCounter;
}

std::vector<std::string>* expandFromClick(std::vector<std::string>* mineMap, int clickX, int clickY){
    if(!isPointInMapRange(mineMap, clickX, clickY) || (*mineMap)[clickY][clickX] != BLANK_CHAR){
        return mineMap;
    }

    (*mineMap)[clickY][clickX] = std::to_string(numberOfNeighborMines(mineMap, clickX, clickY))[0];
    if(numberOfNeighborMines(mineMap, clickX, clickY) > 0){
        return mineMap;
    }else{
        expandFromClick(mineMap, clickX+1, clickY);
        expandFromClick(mineMap, clickX+1, clickY+1);
        expandFromClick(mineMap, clickX+1, clickY-1);
        expandFromClick(mineMap, clickX-1, clickY);
        expandFromClick(mineMap, clickX-1, clickY+1);
        expandFromClick(mineMap, clickX-1, clickY-1);
        expandFromClick(mineMap, clickX, clickY+1);
        expandFromClick(mineMap, clickX, clickY-1);
    }
    return mineMap;
}

std::vector<std::string>* hideBlanksAndMines(std::vector<std::string>* mineMap){
    for (unsigned int i = 0; i < mineMap->size(); ++i) {
        for (unsigned int j = 0; j < (*mineMap)[i].length(); ++j) {
            if((*mineMap)[i][j] == BLANK_CHAR || (*mineMap)[i][j] == MINE_CHAR){
                (*mineMap)[i][j] = HIDDEN_CHAR;
            }
        }
    }
    return mineMap;
}

int main()
{
    int numberOfRows,  rowLength, clickX, clickY;
    std::cin >> numberOfRows >> rowLength >> clickX >> clickY;
    std::vector<std::string>* mineMap = readInputMap(numberOfRows);
    mineMap = expandFromClick(mineMap, clickX-1, clickY-1);
    mineMap = hideBlanksAndMines(mineMap);

    printMap(mineMap);

    delete(mineMap);

    return 0;
}
